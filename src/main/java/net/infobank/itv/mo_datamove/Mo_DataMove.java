package net.infobank.itv.mo_datamove;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import net.infobank.itv.mo_common.util.CommonUtils;
import net.infobank.itv.mo_common.util.FileConfig;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_datamove.util.BoardUtils;
import net.infobank.itv.mo_datamove.util.ResponseTrlistUtils;
import net.infobank.itv.mo_datamove.util.StatisticsUtils;
import net.infobank.itv.mo_datamove.util.EmSmtTrantUtils;

public class Mo_DataMove {
    private static Logger log = LoggerFactory.getLogger(Mo_DataMove.class);

    public static void main(String[] args) {
        Mo_DataMove data_proc = new Mo_DataMove();
        data_proc.start(args);
    }
    
    public void start(String[] args) {
        FileConfig _config;

        // 접속정보 를 파일에서 가져온다.
        if (args.length < 1) {
            System.err.println("error: not enough argument");
            System.err.println("<arg1:config file>");
            System.err.println("config ymd");
            return;
        }

        _config = new FileConfig(args[0]);
        
        String group_name = new Object() {}.getClass().getEnclosingClass().getSimpleName();
        MDC.put("log_file", _config.getLog_dir() + "/" + group_name+ "/" + group_name);
        
        Properties props = new Properties();
        props.put("driver", _config.getDriver());
        props.put("url", _config.getUrl());
        props.put("user", _config.getUser());
        props.put("password", _config.getPw());
        MyBatisConnectionFactory.setSqlSessionFactory(props);
        
        String version = _config.getVersion(); // 버전 
        int nVersion = Integer.parseInt(version); 
    
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String now = sdf.format(date);
        
        if (args.length > 1) {
            now = args[1];
        }
        
        try {
            int before = -3;
            
            String ymd = CommonUtils.addDay("yyyyMMdd", now, before);
            int count = 0;
            
            if(nVersion == 2 ) { 
                count = BoardUtils.moveDataBoard(ymd);
                log.info("Move Board {} Count {}", ymd, count);
                
                count = EmSmtTrantUtils.moveDataEmSmtTran(ymd);
                log.info("Move EmSmtTran {} Count {}", ymd, count);
            	
            } else {
                count = BoardUtils.moveDataBoard(ymd);
                log.info("Move Board {} Count {}", ymd, count);
                
                count = ResponseTrlistUtils.moveDataResponseTrlist(ymd);
                log.info("Move ResponseTrlist {} Count {}", ymd, count);
                
                before = -1;
                ymd = CommonUtils.addDay("yyyyMMdd", now, before);
                count = StatisticsUtils.moveDataStatistics(ymd);
                log.info("Move Statistics {} Count {}", ymd, count);
            }

        } catch (Exception e) {
            log.error("Exception", e);
        }
    }

}
