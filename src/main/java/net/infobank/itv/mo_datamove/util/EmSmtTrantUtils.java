package net.infobank.itv.mo_datamove.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_datamove.dao.EmSmtTranDAO;
import net.infobank.itv.mo_datamove.dao.ResponseTrlistDAO;

public class EmSmtTrantUtils {
    private static Logger log = LoggerFactory.getLogger(EmSmtTrantUtils.class);
    
    public static int moveDataEmSmtTran(String ymd) {
        int id = -1;
        EmSmtTranDAO emSmtTranDAO = new EmSmtTranDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = emSmtTranDAO.moveDataEmSmtTran(ymd);
        } catch(Exception e)  {
            log.error("emSmtTranDAO.moveDataEmSmtTran : " , e);            
        }
        return id;
    }
}
