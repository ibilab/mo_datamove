package net.infobank.itv.mo_datamove.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_datamove.dao.BoardDAO;

public class BoardUtils {
    private static Logger log = LoggerFactory.getLogger(BoardUtils.class);
    
    public static int moveDataBoard(String ymd) {
        int id = -1;
        BoardDAO boardDAO = new BoardDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = boardDAO.moveDataBoard(ymd);
        } catch(Exception e)  {
            log.error("Board.moveDataBoard : " , e);            
        }
        return id;
    }
}
