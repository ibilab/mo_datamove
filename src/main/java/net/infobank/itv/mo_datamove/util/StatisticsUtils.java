package net.infobank.itv.mo_datamove.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_datamove.dao.StatisticsDAO;

public class StatisticsUtils {
    private static Logger log = LoggerFactory.getLogger(StatisticsUtils.class);
    
    public static int moveDataStatistics(String ymd) {
        int id = -1;
        StatisticsDAO StatisticsDAO = new StatisticsDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = StatisticsDAO.moveDataStatistics(ymd);
        } catch(Exception e)  {
            log.error("Statistics.moveDataStatistics : " , e);            
        }
        return id;
    }
}
