package net.infobank.itv.mo_datamove.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_datamove.dao.ResponseTrlistDAO;

public class ResponseTrlistUtils {
    private static Logger log = LoggerFactory.getLogger(ResponseTrlistUtils.class);
    
    public static int moveDataResponseTrlist(String ymd) {
        int id = -1;
        ResponseTrlistDAO responseTrlistDAO = new ResponseTrlistDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = responseTrlistDAO.moveDataResponseTrlist(ymd);
        } catch(Exception e)  {
            log.error("Board.moveDataResponseTrlist : " , e);            
        }
        return id;
    }
}
