package net.infobank.itv.mo_datamove.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResponseTrlistDAO {
    private static Logger log = LoggerFactory.getLogger(ResponseTrlistDAO.class);
    private SqlSessionFactory sqlSessionFactory = null;

    public ResponseTrlistDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }

    public int moveDataResponseTrlist(String ymd) {
        int count = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("month", ymd.substring(0, 6));
            paramMap.put("day", ymd);
            
            count = session.update("ResponseTrlist.createOldTable", paramMap);
            
            count = session.insert("ResponseTrlist.insertResponseTrlistOld", paramMap);
            count = session.delete("ResponseTrlist.deleteResponseTrlist", paramMap);
            session.commit();
        } catch(Exception e)  {
            log.error("Board.moveDataResponseTrlist : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return count;
    }

}
