package net.infobank.itv.mo_datamove.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StatisticsDAO {
    private static Logger log = LoggerFactory.getLogger(StatisticsDAO.class);
    private SqlSessionFactory sqlSessionFactory = null;

    public StatisticsDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }

    public int moveDataStatistics(String ymd) {
        int count = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("month", ymd.substring(0, 6));
            paramMap.put("day", ymd);
            
            count = session.update("Statistics.createOldTable", paramMap);
            
            count = session.insert("Statistics.insertStatisticsOld", paramMap);
            count = session.delete("Statistics.deleteStatistics", paramMap);
            session.commit();
        } catch(Exception e)  {
            log.error("Statistics.moveDataStatistics : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return count;
    }

}
