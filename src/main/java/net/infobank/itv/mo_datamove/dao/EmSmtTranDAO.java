package net.infobank.itv.mo_datamove.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmSmtTranDAO {
    private static Logger log = LoggerFactory.getLogger(EmSmtTranDAO.class);
    private SqlSessionFactory sqlSessionFactory = null;

    public EmSmtTranDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }

    public int moveDataEmSmtTran(String ymd) {
        int count = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("month", ymd.substring(0, 6));
            paramMap.put("day", ymd);
            
            count = session.update("EmSmtTran.createOldTable", paramMap);
            
            count = session.insert("EmSmtTran.insertEmSmtTranOld", paramMap);
            count = session.delete("EmSmtTran.deleteEmSmtTran", paramMap);
            session.commit();
        } catch(Exception e)  {
            log.error("EmSmtTran.moveDataEmSmtTran : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return count;
    }

}
